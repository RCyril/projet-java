package ProjetJavaRDF.RDF;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.MalformedURLException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.htmlcleaner.XPatherException;
 
public class Fenetre extends JFrame 
{
		private static final long serialVersionUID = 1L;

		private JPanel container = new JPanel();
        
        JPanel top_recherche = new JPanel();
        JLabel top_ecriture = new JLabel();

        JTextField recherche_champ = new JTextField ();
        JButton recherche_button = new JButton();
        JPanel center_panel = new JPanel();
        
        private static String[][] resultatRDF = new String[3][2];	
        
        public Fenetre()
        {
        	this.setSize(1050, 700);
            this.setTitle("Moteur De Recherche Semantique");
            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            this.setLocationRelativeTo(null);
            this.setResizable(false);

            initComposant();
                
            this.setContentPane(container);
            this.setVisible(true);
        }  
        
        private void initComposant()
        {       
        	
        	recherche_champ.setPreferredSize(new Dimension(300, 30));
        	recherche_button.setPreferredSize(new Dimension(150, 30));
        	recherche_button.setText("RECHERCHER");
        	recherche_button.addActionListener(new RechercheListener());
        	
        	//top_recherche.setBackground(Color.green);
        	top_recherche.setPreferredSize(new Dimension(1050, 50));
        	top_recherche.add(top_ecriture,BorderLayout.NORTH);
        	
        	center_panel.setPreferredSize(new Dimension(1050, 600));
        	center_panel.setBackground(Color.white);
        		
        	container.add(recherche_champ,BorderLayout.NORTH);
        	container.add(recherche_button,BorderLayout.NORTH);
        	container.add(top_recherche);
        	container.add(center_panel, BorderLayout.NORTH);
        }
        
        class RechercheListener implements ActionListener
        {
 
             public void actionPerformed(ActionEvent e) 
             {
        		//top_ecriture.setFont(police);
        		//top_ecriture.setPreferredSize(new Dimension(1050, 50));
        		//top_ecriture.setForeground(Color.black); 
        		//top_ecriture.setText(recherche_champ.getText());
        		
        		RechercheRDF recherche = new RechercheRDF(recherche_champ.getText()); 
        		resultatRDF = recherche.getResultat();
        		
        		/*int i=0;
        		int j=0;
        		for(i=0;i<10;i++)
        		{
        			for(j=0;j<2;j++)
            		{
        				System.out.println(i+" "+j+" "+resultatRDF[i][j]);
            		}
        		}*/
        		
        		if(resultatRDF[0][1]=="personnel")
                {
                	ParserProf parseProf;
					try 
					{
						top_ecriture.setText(recherche_champ.getText());
						parseProf = new ParserProf(resultatRDF[1][1]);
						parseProf.etude();
						JPanel panelProf = new JPanel();
	                	panelProf = parseProf.resultat();
	                	center_panel.removeAll();
	        			center_panel.add(panelProf);
	        			center_panel.repaint();
					} 
					catch (MalformedURLException e1) 
					{
						e1.printStackTrace();
					} 
					catch (IOException e1) 
					{
						e1.printStackTrace();
					} 
					catch (XPatherException e1) 
					{
						e1.printStackTrace();
					}
                }
        		else if(resultatRDF[0][1]=="diplome")
                {
        			ParserDiplome parseDiplome;
					try 
					{
						top_ecriture.setText(recherche_champ.getText());
						parseDiplome = new ParserDiplome(resultatRDF[2][1],resultatRDF[3][1],resultatRDF[1][1]);
						parseDiplome.etude();
						JPanel panelDiplome = new JPanel();
	        			panelDiplome = parseDiplome.resultat();
	        			center_panel.removeAll();
	        			center_panel.add(panelDiplome);
	        			center_panel.repaint();
					}
					catch (IOException e1) 
					{
						e1.printStackTrace();
					} 
					catch (XPatherException e1) 
					{
						e1.printStackTrace();
					}
        			//ParserComposante parseComposante = new ParserComposante(resultatRDF[0][1]);	
                }
        		else if(resultatRDF[0][1]=="composante")
        		{
        			ParserComposante parseComposante;
					try 
					{
						top_ecriture.setText(recherche_champ.getText());
						parseComposante = new ParserComposante(resultatRDF[1][1]);
						parseComposante.etude();
						JPanel panelComposante = new JPanel();
	        			panelComposante= parseComposante.resultat();
	        			center_panel.removeAll();
	        			center_panel.add(panelComposante);
	        			center_panel.repaint();
					} 
					catch (IOException e1) 
					{
						e1.printStackTrace();
					} 
					catch (XPatherException e1) 
					{
						e1.printStackTrace();
					}
        		}
            }
        }
}
