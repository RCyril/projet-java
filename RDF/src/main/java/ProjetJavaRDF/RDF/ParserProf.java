package ProjetJavaRDF.RDF;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Vector;

import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.htmlcleaner.XPatherException;


public class ParserProf
{
	private JPanel resultat = new JPanel();
	/* Unit� de formation de science */
	private static String unitFormSc = "http://applis.uvsq.fr/AnnuairePersonnel/ResultatEmploye?CheckFieldName=COD_CMP&CheckLabelID=COD_CMP_CheckLabel&CallbackScript=&Composante=S&Service=0&SousService=0&ListID=Service_Liste&LabelID=Service_Label&type=composante&structfin=0&Previous_action=RechercheStructure&COD_CMP=S&COD_SVE=0";
	
	/* Unit� de formation de science politique/droit*/
	private static String unitFormScPo = "http://applis.uvsq.fr/AnnuairePersonnel/ResultatEmploye?CheckFieldName=COD_CMP&CheckLabelID=COD_CMP_CheckLabel&CallbackScript=&Composante=J&Service=0&SousService=0&ListID=Service_Liste&LabelID=Service_Label&type=composante&structfin=0&Previous_action=RechercheStructure&COD_CMP=J&COD_SVE=0";
	
	/* Unit� de formation de science sociale*/
	private static String unitFormScSo = "http://applis.uvsq.fr/AnnuairePersonnel/ResultatEmploye?CheckFieldName=COD_CMP&CheckLabelID=COD_CMP_CheckLabel&CallbackScript=&Composante=SS&Service=0&SousService=0&ListID=Service_Liste&LabelID=Service_Label&type=composante&structfin=0&Previous_action=RechercheStructure&COD_CMP=SS&COD_SVE=0";
	
	/* Unit� de formation de science sociale*/
	private static String unitFormSante = "http://applis.uvsq.fr/AnnuairePersonnel/ResultatEmploye?CheckFieldName=COD_CMP&CheckLabelID=COD_CMP_CheckLabel&CallbackScript=&Composante=A&Service=0&SousService=0&ListID=Service_Liste&LabelID=Service_Label&type=composante&structfin=0&Previous_action=RechercheStructure&COD_CMP=A&COD_SVE=0";
	
	/* UIT Velizy*/
	private static String uitV = "http://applis.uvsq.fr/AnnuairePersonnel/ResultatEmploye?CheckFieldName=COD_CMP&CheckLabelID=COD_CMP_CheckLabel&CallbackScript=&Composante=I&Service=0&SousService=0&ListID=Service_Liste&LabelID=Service_Label&type=composante&structfin=0&Previous_action=RechercheStructure&COD_CMP=I&COD_SVE=0";
	
	/* UIT Mante*/
	private static String uitM = "http://applis.uvsq.fr/AnnuairePersonnel/ResultatEmploye?CheckFieldName=COD_CMP&CheckLabelID=COD_CMP_CheckLabel&CallbackScript=&Composante=M&Service=0&SousService=0&ListID=Service_Liste&LabelID=Service_Label&type=composante&structfin=0&Previous_action=RechercheStructure&COD_CMP=M&COD_SVE=0";
	
	/* UIT ISTY*/
	private static String isty = "http://applis.uvsq.fr/AnnuairePersonnel/ResultatEmploye?CheckFieldName=COD_CMP&CheckLabelID=COD_CMP_CheckLabel&CallbackScript=&Composante=Y&Service=0&SousService=0&ListID=Service_Liste&LabelID=Service_Label&type=composante&structfin=0&Previous_action=RechercheStructure&COD_CMP=Y&COD_SVE=0";
	
	/* Insitut de management*/
	private static String management ="http://applis.uvsq.fr/AnnuairePersonnel/ResultatEmploye?CheckFieldName=COD_CMP&CheckLabelID=COD_CMP_CheckLabel&CallbackScript=&Composante=ISM&Service=0&SousService=0&ListID=Service_Liste&LabelID=Service_Label&type=composante&structfin=0&Previous_action=RechercheStructure&COD_CMP=ISM&COD_SVE=0";
	/* �tudes culturelles */
	private static String etuCulture = "http://applis.uvsq.fr/AnnuairePersonnel/ResultatEmploye?CheckFieldName=COD_CMP&CheckLabelID=COD_CMP_CheckLabel&CallbackScript=&Composante=ICI&Service=0&SousService=0&ListID=Service_Liste&LabelID=Service_Label&type=composante&structfin=0&Previous_action=RechercheStructure&COD_CMP=ICI&COD_SVE=0";
	
	private static String [] mesSources = new String[9];
	private static String prof_rechercher = new String();
	private static String prof_rechercher_base = new String();
	private static String prof1= new String();
	private static String prof2= new String();
	private static String prof3= new String();
	
	public Vector<String[]> resultats = new Vector<String[]>();
	private boolean test;
	
	
	public static String getProf_rechercher() {
		return prof_rechercher;
	}

	public static void setProf_rechercher(String prof_rechercher) {
		ParserProf.prof_rechercher = prof_rechercher;
	}

	public ParserProf(String mot)
	{
		prof_rechercher = mot;
		prof_rechercher_base = mot;
		prof_rechercher = prof_rechercher.replaceAll("_"," ");
	}
	
	public void etude() throws MalformedURLException, IOException, XPatherException
	{
		init();
		int taille = 0;
		boolean bool = false;
		while(taille < mesSources.length && bool == false)
		{
			bool = recherche(prof_rechercher, mesSources[taille]);
			taille++;
			/*if(bool == true)
				//affiche();
			else
				System.out.println("Source "+(taille)+" aucun r�sultat");*/
		}
		
	}
	
	public void init()
	{
		mesSources[0] = unitFormSc;
		mesSources[1] = unitFormScPo;
		mesSources[2] = unitFormScSo;
		mesSources[3] = unitFormSante;
		mesSources[4] = uitV;
		mesSources[5] = uitM;
		mesSources[6] = isty;	
		mesSources[7] = management;
		mesSources[8] = etuCulture;
	}
	
	
	public boolean recherche(String mot, String lien) throws MalformedURLException, IOException, XPatherException 
	{
		HtmlCleaner cleaner = new HtmlCleaner();
		TagNode node = cleaner.clean(new URL(lien));
		boolean bool = false;

		String[] decoupage = mot.split(" ");
		if(decoupage.length == 1)
		{
			for (Object o : node.evaluateXPath("//table[@class='clFlatTable']//tr//td")) 
			{
				if(((((TagNode)(o)).getText()).toString()).toLowerCase().equals(mot.toLowerCase()))
				{
					bool = true;
					int i = 0;
					String [] tabl = new String[6];
					for(Object k : ((TagNode)(o)).getParent().evaluateXPath("td"))
					{
						String str = ((TagNode)(k)).getText().toString();
						tabl[i] = str;
						i++;
					}
					resultats.addElement(tabl);
				}	
			}
		}
		else
		{
			test = false;
			for (Object o : node.evaluateXPath("//table[@class='clFlatTable']//tr//td")) 
			{
				if(((((TagNode)(o)).getText()).toString()).toLowerCase().equals(decoupage[0].toLowerCase()))
				{
					int tag = -1;
					if(((((TagNode)(o)).getText()).toString()).toLowerCase().equals(decoupage[0].toLowerCase()))
						tag = 1;
					else
						tag = 2;
					bool = true;
					int i = 0;
					String [] tabl = new String[6];
					for(Object k : ((TagNode)(o)).getParent().evaluateXPath("td"))
					{
						String str = ((TagNode)(k)).getText().toString();
						if(tag == 1)
						{
							if(str.toLowerCase().equals(decoupage[1].toLowerCase()))
								test = true;
						}
						if(tag == 2)
						{
							if(str.toLowerCase().equals(decoupage[0].toLowerCase()))
								test = true;
						}
						tabl[i] = str;
						i++;
					}
					int count = 0;
					for(String s : tabl)
					{
						if(s.toLowerCase().equals(decoupage[0].toLowerCase()))
							count++;
						if(s.toLowerCase().equals(decoupage[1].toLowerCase()))
							count++;
					}
					if(count == 2)
						resultats.addElement(tabl);
				}
			}
		}
		return bool;
	}
	
	public JPanel resultat() throws MalformedURLException, IOException, XPatherException 
    { 
		JPanel gauche =  new JPanel();
		JPanel droite =  new JPanel();
		
		Font police_base = new Font("Arial", Font.BOLD, 10);
		Font police_titre = new Font("Arial", Font.BOLD, 20);
		
		JLabel titre = new JLabel();
		titre.setFont(police_titre);
		titre.setPreferredSize(new Dimension(500, 50));
		titre.setForeground(Color.black); 
		titre.setText(prof_rechercher);
		
		JTextArea panel_info_principal = new JTextArea();
		panel_info_principal.setFont(police_base);
		panel_info_principal.setPreferredSize(new Dimension(500, 150));
		panel_info_principal.setForeground(Color.black); 
		panel_info_principal.setLineWrap(true);
		panel_info_principal.setWrapStyleWord(true);
		
		if(resultats.size() == 0)
			System.out.println("Aucun resultat");
		else
		{
			/*for(int i = 0; i<resultats.size(); i++)
			{
				System.out.println("-------------------------------------");
				String [] str = resultats.elementAt(i);
				for(String s : str)
					System.out.println(s);
				System.out.println("-------------------------------------");
			}*/
			
			String [] str = resultats.elementAt(0);
			for(String s : str)
			{
				//System.out.println(s);
				panel_info_principal.setText(panel_info_principal.getText()+"\n"+s);
			}
		}		
		
		gauche.setBackground(Color.white);
    	gauche.setPreferredSize(new Dimension(500, 630));
    	gauche.add(titre,BorderLayout.NORTH);
    	gauche.add(panel_info_principal,BorderLayout.NORTH);
		
    	JPanel panel_uvsq = new JPanel();
		panel_uvsq.setBackground(Color.white);
		panel_uvsq.setPreferredSize(new Dimension(530, 280));
		
		java.net.URL where;
		try 
		{
			where = new URL("http://www.uvsq.fr/images/logo.png");
			ImageIcon logo = new ImageIcon(where);
			ImagePanel logo_uvsq = new ImagePanel(logo.getImage());
			panel_uvsq.add(logo_uvsq,BorderLayout.CENTER);
		} catch (MalformedURLException e) 
		{
			e.printStackTrace();
		}
		
		ParserUVSQ p = new ParserUVSQ();
		JTextArea info_UVSQ = new JTextArea();
		info_UVSQ.setFont(police_base);
		info_UVSQ.setPreferredSize(new Dimension(500, 200));
		info_UVSQ.setForeground(Color.black); 
		info_UVSQ.setLineWrap(true);
		info_UVSQ.setWrapStyleWord(true);
		info_UVSQ.setText(p.getInfos());
		panel_uvsq.add(info_UVSQ,BorderLayout.NORTH);
		
		JPanel panel_prof = new JPanel();
		panel_prof.setBackground(Color.white);
		panel_prof.setPreferredSize(new Dimension(530, 280));
		
		RechercheRDF temp = new RechercheRDF(prof_rechercher_base);
		prof1 = temp.getProfesseur(prof_rechercher_base).get(0);
		ParserProf p1 = new ParserProf(prof1);
		JPanel panel_prof1 = new JPanel();
		panel_prof1.setBackground(Color.white);
		panel_prof1.setPreferredSize(new Dimension(170, 280));
		JTextArea nom_prof1= new JTextArea(p1.getProf_rechercher());
		nom_prof1.setPreferredSize(new Dimension(170, 180));
		nom_prof1.setLineWrap(true);
		nom_prof1.setWrapStyleWord(true); 
		JButton button_prof1 = new JButton();
		button_prof1.setPreferredSize(new Dimension(150, 30));
		button_prof1.setText("ACCEDER");
		button_prof1.addActionListener(new prof1Listener());
		panel_prof1.add(nom_prof1);
		panel_prof1.add(button_prof1);
		
		prof2 = temp.getProfesseur(prof_rechercher_base).get(1);
		ParserProf p2 = new ParserProf(prof2);
		JPanel panel_prof2 = new JPanel();
		panel_prof2.setBackground(Color.white);
		panel_prof2.setPreferredSize(new Dimension(170, 280));
		JTextArea nom_prof2= new JTextArea(p2.getProf_rechercher());
		nom_prof2.setPreferredSize(new Dimension(170, 180));
		nom_prof2.setLineWrap(true);
		nom_prof2.setWrapStyleWord(true); 
		JButton button_prof2 = new JButton();
		button_prof2.setPreferredSize(new Dimension(150, 30));
		button_prof2.setText("ACCEDER");
		button_prof2.addActionListener(new prof2Listener());
		panel_prof2.add(nom_prof2);
		panel_prof2.add(button_prof2);
		
		prof3 = temp.getProfesseur(prof_rechercher_base).get(2);
		ParserProf p3 = new ParserProf(prof1);
		JPanel panel_prof3 = new JPanel();
		panel_prof3.setBackground(Color.white);
		panel_prof3.setPreferredSize(new Dimension(170, 280));
		JTextArea nom_prof3= new JTextArea(p3.getProf_rechercher());
		nom_prof3.setPreferredSize(new Dimension(170, 180));
		nom_prof3.setLineWrap(true);
		nom_prof3.setWrapStyleWord(true); 
		JButton button_prof3 = new JButton();
		button_prof3.setPreferredSize(new Dimension(150, 30));
		button_prof3.setText("ACCEDER");
		button_prof3.addActionListener(new prof3Listener());
		panel_prof3.add(nom_prof3);
		panel_prof3.add(button_prof3);
		
		panel_prof.add(panel_prof1,BorderLayout.EAST);
		panel_prof.add(panel_prof2,BorderLayout.EAST);
		panel_prof.add(panel_prof3,BorderLayout.EAST);
    	
    	//droite.setBackground(Color.white);
    	droite.setPreferredSize(new Dimension(530, 640));
    	droite.add(panel_uvsq,BorderLayout.NORTH);
    	droite.add(panel_prof,BorderLayout.NORTH);
    	
		//resultat.setBackground(Color.white);
    	resultat.setPreferredSize(new Dimension(1050, 650));
    	resultat.add(gauche,BorderLayout.EAST);
    	resultat.add(droite,BorderLayout.WEST);
		return resultat;
    }
	
	class prof1Listener implements ActionListener
    {
        public void actionPerformed(ActionEvent e) 
        {
			try {
				resultat.removeAll();
				ParserProf p1;
				System.out.println("NOUVELLE PAGE "+prof1);
				p1 = new ParserProf(prof1);
				p1.etude();
				JPanel panelNewProf = new JPanel();
				panelNewProf = p1.resultat();
				BorderLayout test = new BorderLayout();
				resultat.add(panelNewProf,test);
				resultat.validate();
				resultat.repaint();
			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (XPatherException e1) {
				e1.printStackTrace();
			}
        }
    }
	class prof2Listener implements ActionListener
    {
        public void actionPerformed(ActionEvent e) 
        {
			try {
				resultat.removeAll();
				ParserProf p2;
				System.out.println("NOUVELLE PAGE "+prof2);
				p2 = new ParserProf(prof2);
				p2.etude();
				JPanel panelNewProf = new JPanel();
				panelNewProf = p2.resultat();
				BorderLayout test = new BorderLayout();
				resultat.add(panelNewProf,test);
				resultat.validate();
				resultat.repaint();
			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (XPatherException e1) {
				e1.printStackTrace();
			}
        }
    }
	class prof3Listener implements ActionListener
    {
         public void actionPerformed(ActionEvent e) 
         {
			try {
				resultat.removeAll();
				ParserProf p3;
				System.out.println("NOUVELLE PAGE "+prof3);
				p3 = new ParserProf(prof3);
				p3.etude();
				JPanel panelNewProf = new JPanel();
				panelNewProf = p3.resultat();
				BorderLayout test = new BorderLayout();
				resultat.add(panelNewProf,test);
				resultat.validate();
				resultat.repaint();
			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (XPatherException e1) {
				e1.printStackTrace();
			}
         }
    }
	
	/*public void affiche()
	{
		if(resultats.size() == 0)
			System.out.println("Aucun resultat");
		else
		{
			for(int i = 0; i<resultats.size(); i++)
			{
				System.out.println("-------------------------------------");
				String [] str = resultats.elementAt(i);
				for(String s : str)
					System.out.println(s);
				System.out.println("-------------------------------------");
			}
		}		
	}*/

	public Vector<String[]> getResultat() {
		return resultats;
	}
}


/*public class ParserProf 
{
	String prof_rechercher; 
	public ParserProf(String rech)
    {
		prof_rechercher = rech;
    } 
	 
	public JPanel resultat() 
    { 
		JPanel resultat = new JPanel();
		
		JLabel titre = new JLabel();
		Font police = new Font("Arial", Font.BOLD, 10);
		titre.setFont(police);
		titre.setPreferredSize(new Dimension(1050, 50));
		titre.setForeground(Color.black); 
		titre.setText("Professeur");
		
		resultat.setBackground(Color.red);
    	resultat.setPreferredSize(new Dimension(1050, 530));
    	resultat.add(titre,BorderLayout.NORTH);
		return resultat;
    }
}*/
