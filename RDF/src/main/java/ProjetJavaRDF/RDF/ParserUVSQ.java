package ProjetJavaRDF.RDF;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.htmlcleaner.XPatherException;

public class ParserUVSQ 
{
	private static String source = "http://fr.wikipedia.org/wiki/Universit%C3%A9_de_Versailles-Saint-Quentin-en-Yvelines";
	private static String info;
	public ParserUVSQ() throws MalformedURLException, IOException, XPatherException
	{
		recherche(source);
	}
	
	public void recherche(String lien) throws MalformedURLException, IOException, XPatherException 
	{
		HtmlCleaner cleaner = new HtmlCleaner();
		TagNode node = cleaner.clean(new URL(lien));
		info = new String();
		int count = 0;
		
		for (Object o : node.evaluateXPath("//body//p")) 
		{
			if(count > 2  && count < 4){
				info = info + ((TagNode)(o)).getText() + "\n";
			}
			count++;
		}
		//System.out.println(info);
	}
	
	public String getInfos(){return info;}
	
}