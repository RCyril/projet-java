package ProjetJavaRDF.RDF;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;

class Tuple{
	public String type;
	public List<String> mots = new ArrayList<String>();
	public int nb = 0;
}

class lTuple{
	List<Tuple> ltuples = new ArrayList<Tuple>();
	
	public List<Tuple> getLtuples() {
		return ltuples;
	}
	public void setLtuples(List<Tuple> ltuples) {
		this.ltuples = ltuples;
	}
	public void addLtuples (String mot, String type){
		boolean add = false;
		for(Tuple t:ltuples){
			if(t.type.equals(type)){
				t.mots.add(mot);
				t.nb ++;
				add = true;
			}
		}
		if(!add){
			Tuple t = new Tuple();
			t.type = type;
			t.mots.add(mot);
			t.nb ++;
			add = true;
			ltuples.add(t);
		}
	}
	public void affichage(){
		for(Tuple t:ltuples){
			System.out.println(t.type +" : "+ t.nb);
			for(String mot:t.mots){
				System.out.println(" * "+ mot);
			}
		}
	}
	
	public List<String> getType(String type){
		for(Tuple t:ltuples){
			if(t.type.equals(type)){
				return t.mots;
			}
		}
		return null;
	}
	
	public String getMaxType(){
		int max = 0;
		for(Tuple t:ltuples){
			if(t.nb> max) max = t.nb;
		}
		for(Tuple t:ltuples){
			if(t.nb == max) return t.type;
		}
		return null;
	}
	public String getMaxType(String tt){
		int max = 0;
		for(Tuple t:ltuples){
			if(!tt.equals(t.type) && t.nb> max) max = t.nb;
		}
		for(Tuple t:ltuples){
			if(t.nb == max && !tt.equals(t.type)) return t.type;
		}
		return null;
	}
	
}

public class RechercheRDF {
	
	String Prefix = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> " +
					"PREFIX owl: <http://www.w3.org/2002/07/owl#> " +
					"PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> " +
					"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " +
					"PREFIX uvsq: <http://www.ontologie2013.com/rclmbyffdn#> ";
	
	String[] mots;
	lTuple lt = new lTuple();
	//Model model = ModelFactory.createDefaultModel().read("uvsq2.owl");
	Model model = ModelFactory.createDefaultModel().read("uvsq2.owl");
	
	public RechercheRDF(String requete){
		
		requete = requete.replace(" et ", " ");
		requete = requete.replace(" de ", " ");
		requete = requete.replace(" des ", " ");
		requete = requete.replace(" en ", " ");
		requete = requete.replace(" un ", " ");
		requete = requete.replace(" une ", " ");
		requete = requete.replace(" le ", " ");
		requete = requete.replace(" la ", " ");
		requete = requete.replace(" les ", " ");
		requete = requete.replace(" dans ", " ");
		requete = requete.replace(" l'", " ");
		
		mots = requete.trim().split(" ");
		String SparqlType;
		
		for (String mot:mots){
			//System.out.println(mot);
			SparqlType = Prefix + "SELECT Distinct ?type WHERE { ?a rdf:type ?type. FILTER REGEX(str(?a), '" + mot +"','i'). }";
			System.out.println(SparqlType);
			Query query = QueryFactory.create(SparqlType) ;
	        QueryExecution qexec = QueryExecutionFactory.create(query, model) ;
	        ResultSet results = qexec.execSelect();
	        while(results.hasNext()){
	    		QuerySolution rs = results.nextSolution();
	    		Resource type = rs.getResource("type");
	    		if(!type.getLocalName().equals("NamedIndividual") && !type.getLocalName().equals("ObjectProperty")){
	    			//System.out.println("* "+type.getLocalName());
	    			lt.addLtuples(mot, type.getLocalName());
	    			
	    		}
	        }
		}
		lt.affichage();
		//System.out.println("*"+lt.getMaxType()+"*");
	}
	
	String getClass(String type){
		//System.out.println(type);
		if(type.contains("Composante")){
    		return "Composante";
    	}
    	else if(type.contains("Personnel")){
    		return "Personnel";
    	}
    	else if(type.contains("Diplome")){
    		return "Diplome";
    	}
    	else if(type.contains("Departement")){
    		return "Departement";
    	}
    	else if(type.contains("UVSQ") || type.contains("uvsq")){
        	return "UVSQ";
    	}
		
		String SparqlClass = Prefix + "SELECT Distinct ?classa ?classb WHERE { uvsq:"+type+" rdfs:subClassOf ?classa. OPTIONAL {?classa rdfs:subClassOf ?classb.} }";
		//System.out.println(SparqlClass);
		Query query = QueryFactory.create(SparqlClass) ;
        QueryExecution qexec = QueryExecutionFactory.create(query, model) ;
        ResultSet results = qexec.execSelect();
        if(results.hasNext()){
        	QuerySolution rs = results.nextSolution();
        	Resource classa = rs.getResource("classa");
        	if(classa.getLocalName().contains("Composante")){
        		return "Composante";
        	}
        	else if(classa.getLocalName().contains("Personnel")){
        		return "Personnel";
        	}
        	else if(classa.getLocalName().contains("Diplome")){
        		return "Diplome";
        	}
        	else if(classa.getLocalName().contains("Departement")){
        		return "Departement";
        	}
        	else {
        		Resource classb = rs.getResource("classb");
        		if(classb.getLocalName().contains("Composante")){
            		return "Composante";
            	}
            	else if(classb.getLocalName().contains("Personnel")){
            		return "Personnel";
            	}
            	else if(classb.getLocalName().contains("Diplome")){
            		return "Diplome";
            	}
            	else if(classb.getLocalName().contains("Departement")){
            		return "Departement";
            	}
        	}
        }
        return "";
	}
	
	
	public String[][] getResultat(){
		
		String s = null;
		if(lt.getLtuples().get(0).type.equals("Class")){
			if(lt.getLtuples().get(0).mots.size()>1) return null;
			String Req = Prefix + "SELECT DISTINCT ?b WHERE { ?b rdf:type owl:Class. FILTER REGEX(str(?b), '"+ lt.getLtuples().get(0).mots.get(0) +"', 'i'). } ";
			Query query = QueryFactory.create(Req) ;
	        QueryExecution qexec = QueryExecutionFactory.create(query, model) ;
	        ResultSet results = qexec.execSelect();
	        String clss = null;
	        if(results.hasNext()){
	        	QuerySolution rs = results.nextSolution();
	        	Resource b = rs.getResource("b");
	        	clss = b.getLocalName();
	        }
			s = getClass(clss);
			//System.out.println(s);
		}
		
		String type = lt.getMaxType();
		if(type.equals("Class")){
			String tt = lt.getMaxType("Class");
			System.out.println("tt "+tt);
			if(tt!=null) type = tt;
		}
		//System.out.println("+"+getClass(type)+"+");
		
		if(getClass(type)=="Diplome")
		{
			System.out.println("+"+getClass(type)+"+ type:"+type);
			if(s!=null && s.equals("Personnel")) return getPersonnelDiplome(lt.getType(type));
			return getDiplome(lt.getType(type));
		}
		else if(getClass(type)=="Departement")
		{
			System.out.println("+"+getClass(type)+"+");
			return getDepartement(lt.getType(type));
		}
		else if(getClass(type)=="Personnel")
		{
			System.out.println("+"+getClass(type)+"+");
			return getPersonnel(lt.getType(type));
		}
		else if(getClass(type)=="Composante")
		{
			System.out.println("+"+getClass(type)+"+ type:"+type);
			if(s!=null && s.equals("Personnel")) return getPersonnelComposante(lt.getType(type));
			return getComposante(lt.getType(type));
		}
		else
		{
			System.out.println("+Class+");
			if(s.equals("Diplome")) return getRandDiplome();
			if(s.equals("Departement")) return getRandDepartement();
			if(s.equals("Personnel")) return getRandPersonnel();
			if(s.equals("Composante")) return getRandComposante();
			return null;
		}
	}

	private String[][] getPersonnelComposante(List<String> words) {
		String[][] resultatRDF = new String[10][10];
		resultatRDF[0][0] = "parseur"; resultatRDF[0][1] = "personnel";
		String Req = Prefix + "SELECT DISTINCT ?prof WHERE { ?prof uvsq:appartient ?a. ";
		for(String mot: words){
			Req += "FILTER REGEX(str(?a), '"+ mot +"', 'i'). ";
			System.out.println(mot);
		}
		Req += "}";
		System.out.println(Req);
		Query query = QueryFactory.create(Req) ;
        QueryExecution qexec = QueryExecutionFactory.create(query, model) ;
        ResultSet results = qexec.execSelect();
        List<String> profs = new ArrayList<String>();
        while(results.hasNext()){
        	QuerySolution rs = results.nextSolution();
        	Resource prof = rs.getResource("prof");
        	profs.add(prof.getLocalName());
        }
        int random = (int)(Math.random() * profs.size()-1);
        resultatRDF[1][0] = "professeur"; resultatRDF[1][1] = profs.get(random);
		return resultatRDF;
	}

	private String[][] getPersonnelDiplome(List<String> words) {
		String[][] resultatRDF = new String[10][10];
		resultatRDF[0][0] = "parseur"; resultatRDF[0][1] = "personnel";
		String Req = Prefix + "SELECT DISTINCT ?prof WHERE { ?prof uvsq:donne_des_cours ?a. ";
		for(String mot: words){
			Req += "FILTER REGEX(str(?a), '"+ mot +"', 'i'). ";
			//System.out.println("----"+mot);
		}
		Req += "}";
		System.out.println(Req);
		Query query = QueryFactory.create(Req) ;
        QueryExecution qexec = QueryExecutionFactory.create(query, model) ;
        ResultSet results = qexec.execSelect();
        List<String> profs = new ArrayList<String>();
        while(results.hasNext()){
        	QuerySolution rs = results.nextSolution();
        	Resource prof = rs.getResource("prof");
        	profs.add(prof.getLocalName());
        }
        int random = (int)(Math.random() * profs.size()-1);
        resultatRDF[1][0] = "professeur"; resultatRDF[1][1] = profs.get(random);
		return resultatRDF;
	}

	private String[][] getRandComposante() {
		// TODO Auto-generated method stub
		return null;
	}

	private String[][] getRandPersonnel() {
		// TODO Auto-generated method stub
		return null;
	}

	private String[][] getRandDepartement() {
		// TODO Auto-generated method stub
		return null;
	}

	private String[][] getRandDiplome() {
		// TODO Auto-generated method stub
		return null;
	}

	private String[][] getComposante(List<String> mots) {
		String[][] resultatRDF = new String[10][10];
		resultatRDF[0][0] = "parseur"; resultatRDF[0][1] = "composante";
		String Req = Prefix + "SELECT DISTINCT ?b WHERE { ?b rdf:type uvsq:Composante. ";
		for(String mot: mots){
			Req += "FILTER REGEX(str(?b), '"+ mot +"', 'i'). ";
			System.out.println(mot);
		}
		Req += "}";
		System.out.println(Req);
		Query query = QueryFactory.create(Req) ;
        QueryExecution qexec = QueryExecutionFactory.create(query, model) ;
        ResultSet results = qexec.execSelect();
        boolean fisrt = true;
        while(results.hasNext()){
        	QuerySolution rs = results.nextSolution();
        	Resource b = rs.getResource("b");
        	if(fisrt){
        		resultatRDF[1][0] = "composante"; resultatRDF[1][1] = b.getLocalName();
        		fisrt = false;
        	}
        	else if(b.getLocalName().length() < resultatRDF[1][1].length()){
        		resultatRDF[1][0] = "composante"; resultatRDF[1][1] = b.getLocalName();
        	}
        }
		return resultatRDF;
	}

	private String[][] getPersonnel(List<String> mots) {
		String[][] resultatRDF = new String[10][10];
		resultatRDF[0][0] = "parseur"; resultatRDF[0][1] = "personnel";
		String Req = Prefix + "SELECT DISTINCT ?b WHERE { ?b rdf:type uvsq:Personnel. ";
		for(String mot: mots){
			Req += "FILTER REGEX(str(?b), '"+ mot +"', 'i'). ";
			System.out.println(mot);
		}
		Req += "}";
		System.out.println(Req);
		Query query = QueryFactory.create(Req) ;
        QueryExecution qexec = QueryExecutionFactory.create(query, model) ;
        ResultSet results = qexec.execSelect();
        boolean fisrt = true;
        while(results.hasNext()){
        	QuerySolution rs = results.nextSolution();
        	Resource b = rs.getResource("b");
        	if(fisrt){
        		resultatRDF[1][0] = "professeur"; resultatRDF[1][1] = b.getLocalName();
        		fisrt = false;
        	}
        	else if(b.getLocalName().length() < resultatRDF[1][1].length()){
        		resultatRDF[1][0] = "professeur"; resultatRDF[1][1] = b.getLocalName();
        	}
        		
        }
		return resultatRDF;
	}

	private String[][] getDepartement(List<String> mots) {
		String[][] resultatRDF = new String[10][10];
		resultatRDF[0][0] = "parseur"; resultatRDF[0][1] = "departement";
		String Req = Prefix + "SELECT DISTINCT ?b WHERE { ?b rdf:type uvsq:Departement. ";
		for(String mot: mots){
			Req += "FILTER REGEX(str(?b), '"+ mot +"', 'i'). ";
			System.out.println(mot);
		}
		Req += "}";
		System.out.println(Req);
		Query query = QueryFactory.create(Req) ;
        QueryExecution qexec = QueryExecutionFactory.create(query, model) ;
        ResultSet results = qexec.execSelect();
        boolean fisrt = true;
        while(results.hasNext()){
        	QuerySolution rs = results.nextSolution();
        	Resource b = rs.getResource("b");
        	if(fisrt){
        		resultatRDF[1][0] = "professeur"; resultatRDF[1][1] = b.getLocalName();
        		fisrt = false;
        	}
        	else if(b.getLocalName().length() < resultatRDF[1][1].length()){
        		resultatRDF[1][0] = "professeur"; resultatRDF[1][1] = b.getLocalName();
        	}
        		
        }
		return resultatRDF;
	}

	private String[][] getDiplome(List<String> mots) {
		String[][] resultatRDF = new String[10][10];
		resultatRDF[0][0] = "parseur"; resultatRDF[0][1] = "diplome";
		String Req = Prefix + "SELECT DISTINCT ?b ?d ?e WHERE { ?d rdfs:subClassOf uvsq:Diplome. ?b rdf:type ?d. ?b uvsq:est_dispense_dans ?e. OPTIONAL{?b rdf:type ?d.} OPTIONAL{?b rdf:type uvsq:Diplome.} ";
		for(String mot: mots){
			Req += "FILTER REGEX(str(?b), '"+ mot +"', 'i'). ";
			//System.out.println(mot);
		}
		Req += "}";
		System.out.println(Req);
		Query query = QueryFactory.create(Req) ;
        QueryExecution qexec = QueryExecutionFactory.create(query, model) ;
        ResultSet results = qexec.execSelect();
        boolean fisrt = true;
        while(results.hasNext()){
        	QuerySolution rs = results.nextSolution();
        	Resource b = rs.getResource("b");
        	Resource d = rs.getResource("d");
        	Resource e = rs.getResource("e");
        	if(fisrt){
	        	resultatRDF[1][0] = "composante"; resultatRDF[1][1] = e.getLocalName();
	        	resultatRDF[2][0] = "niveau"; resultatRDF[2][1] = d.getLocalName();
	        	resultatRDF[3][0] = "diplome"; resultatRDF[3][1] = b.getLocalName();
	        	fisrt = false;
        	}
        	else if(b.getLocalName().length() < resultatRDF[3][1].length()){
        		resultatRDF[1][0] = "composante"; resultatRDF[1][1] = e.getLocalName();
	        	resultatRDF[2][0] = "niveau"; resultatRDF[2][1] = d.getLocalName();
	        	resultatRDF[3][0] = "diplome"; resultatRDF[3][1] = b.getLocalName();
        	}
        }
		return resultatRDF;
	}
	
	public List<String> getComposante(String composante){
		List<String> retour = new ArrayList<String>();
		String Req = Prefix + "SELECT DISTINCT ?comp WHERE { ?comp rdf:type uvsq:Composante. } ";
		
		List<String> res = new ArrayList<String>();
		
		Query query = QueryFactory.create(Req) ;
        QueryExecution qexec = QueryExecutionFactory.create(query, model) ;
        ResultSet results = qexec.execSelect();
        while(results.hasNext()){
        	QuerySolution rs = results.nextSolution();
        	Resource comp = rs.getResource("comp");
        	if(!composante.equals((comp.getLocalName()))){
        		res.add(comp.getLocalName());
        	}
        }
        
        
        List<Integer> lr = new ArrayList<Integer>();
        
        while(lr.size()<3){
        	int random = (int)(Math.random() * res.size()-1);
        	if(!lr.contains(random)){
        		System.out.println(random);
        		lr.add(random);
        	}
        }
        
        int i=0;
        for(int r:lr){
        	retour.add(res.get(r));
        	if(i>100) break;
        	i++;
        }
		
		return retour;
	}
	
	public List<String[]> getDiplome(String composante){
		List<String[]> retour = new ArrayList<String[]>();
		
		String Req = Prefix + "SELECT DISTINCT ?dipl ?niv WHERE { ?dipl uvsq:est_dispense_dans uvsq:"+composante+". ?dipl rdf:type ?niv.  } ";
		//System.out.println(Req);
		List<String[]> res = new ArrayList<String[]>();
		
		Query query = QueryFactory.create(Req) ;
        QueryExecution qexec = QueryExecutionFactory.create(query, model) ;
        ResultSet results = qexec.execSelect();
        while(results.hasNext()){
        	QuerySolution rs = results.nextSolution();
        	Resource dipl = rs.getResource("dipl");
        	Resource niv = rs.getResource("niv");
        	if(!niv.getLocalName().equals(("NamedIndividual"))){
        		String temp[] = new String[2];
        		temp[0] = dipl.getLocalName();
        		temp[1] = niv.getLocalName();
        		System.out.println(temp[0] + "+" + temp[1]);
        		res.add(temp);
        	}
        }
        
        
        List<Integer> lr = new ArrayList<Integer>();
        
        while(lr.size()<3){
        	int random = (int)(Math.random() * res.size()-1);
        	if(!lr.contains(random)){
        		System.out.println(random);
        		lr.add(random);
        	}
        }
        
        int i=0;
        for(int r:lr){
        	retour.add(res.get(r));
        	if(i>100) break;
        	i++;
        }
		
		return retour;
	}
	
	public List<String> getProfesseur(String professeur){
		List<String> retour = new ArrayList<String>();
		String Req = Prefix + "SELECT DISTINCT ?prof WHERE { ?prof uvsq:appartient ?comp. uvsq:"+professeur+" uvsq:appartient ?comp. } ";
		
		List<String> res = new ArrayList<String>();
		
		Query query = QueryFactory.create(Req) ;
        QueryExecution qexec = QueryExecutionFactory.create(query, model) ;
        ResultSet results = qexec.execSelect();
        while(results.hasNext()){
        	QuerySolution rs = results.nextSolution();
        	Resource prof = rs.getResource("prof");
        	if(!professeur.equals(prof.getLocalName())){
        		res.add(prof.getLocalName());
        	}
        }
        
        
        List<Integer> lr = new ArrayList<Integer>();
        
        while(lr.size()<3){
        	int random = (int)(Math.random() * res.size()-1);
        	if(!lr.contains(random)){
        		System.out.println(random);
        		lr.add(random);
        	}
        }
        
        int i=0;
        for(int r:lr){
        	retour.add(res.get(r));
        	if(i>100) break;
        	i++;
        }
		
		return retour;
	}

}
