package ProjetJavaRDF.RDF;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import org.htmlcleaner.*;

public class ParserDiplome 
{	
	
	JPanel resultat = new JPanel();
	
	private static String recherche_type= new String();
	private static String recherche_diplome= new String();
	private static String recherche_diplome_affichage= new String();
	private static String[] diplome1= new String[2];
	private static String[] diplome2= new String[2];
	private static String[] diplome3= new String[2];
	private static String recherche_composante= new String();
	private static String lien_final= new String();
	
	private static String LiensRecherche= new String();
	private static String LiensLicence = "http://www.uvsq.fr/les-licences-234428.kjsp?RH=FORM_5&RF=FORM_04";
	private static String LiensMaster1 ="http://www.uvsq.fr/les-masters-du-domaine-arts-lettres-langues-234471.kjsp?RH=FORM_5";
	private static String LiensMaster2 ="http://www.uvsq.fr/les-masters-du-domaine-sciences-humaines-et-sociales-234479.kjsp?RH=FORM_5";	
	private static String LiensMaster3 ="http://www.uvsq.fr/les-masters-du-domaine-droit-economie-gestion-234473.kjsp?RH=FORM_5";
	private static String LiensMaster4 ="http://www.uvsq.fr/les-masters-du-domaine-sciences-de-l-environnement-du-territoire-et-de-l-economie-234477.kjsp?RH=FORM_5";
	private static String LiensMaster5 ="http://www.uvsq.fr/les-masters-du-domaine-science-technologie-sante-234475.kjsp?RH=FORM_5";
	private static String LiensDUT ="http://www.uvsq.fr/les-dut-diplome-universitaire-de-technologie-234204.kjsp?RH=FORM_04&RF=FORM6";
	private static String LiensDoctorat ="http://www.uvsq.fr/faire-sa-these-a-l-uvsq-986.kjsp?RH=1362396154791&RF=FORM12";
	private static String LiensMedecine ="http://www.uvsq.fr/la-medecine-233659.kjsp?RH=FORM12&RF=FORM5";
	private static String LiensIngenieur = "http://www.uvsq.fr/les-formations-d-ingenieurs-234293.kjsp?RH=FORM5&RF=FORM7";
	private static String LiensPrepa ="http://www.uvsq.fr/les-preparations-aux-concours-234570.kjsp?RH=FORM7&RF=FORM_15";
	private static String LiensDAEU ="http://www.uvsq.fr/les-daeu-diplome-d-acces-aux-etudes-universitaires--234151.kjsp?RH=FORM_15&RF=1273570711249";
	private static String LiensDU ="http://www.uvsq.fr/les-diplomes-d-universite-du--234174.kjsp?RH=1273570711249&RF=1319114610480";
	private static String LiensDIU ="http://www.uvsq.fr/les-diu-diplomes-inter-universitaires--234186.kjsp?RH=1319114610480&RF=1319126059333";
	private static String LiensCertificat ="http://www.uvsq.fr/les-certifications-262292.kjsp?RH=1319126059333&RF=1362396154791";
	
	private static String[][] info_principal = new String[30][2];	
	private static String[][] description = new String[30][2];
	private static String[][] coordonnees = new String[30][2];
	
	public ParserDiplome (String rech_niveau,String rech_diplome,String composante_appro)
    {
		info_principal = new String[30][2];
		description = new String[30][2];
		coordonnees = new String[30][2];
		recherche_type = rech_niveau;
		recherche_diplome = rech_diplome;
		recherche_diplome_affichage = rech_diplome;
		recherche_composante = composante_appro;
		
		System.out.println("recherche diplome "+recherche_diplome + " niveau "+ recherche_type);
		recherche_diplome_affichage = recherche_diplome_affichage.replaceAll("_", " ");
		recherche_diplome_affichage = recherche_diplome_affichage.replaceAll("0", "'");
		recherche_diplome_affichage = recherche_diplome_affichage.replaceAll("7", "(");
		recherche_diplome_affichage = recherche_diplome_affichage.replaceAll("8", ")");
		recherche_diplome_affichage = recherche_diplome_affichage.replaceAll("9", ",");
		recherche_diplome = recherche_diplome.replaceAll("_", " ");
		recherche_diplome = recherche_diplome.replaceAll("à", "?");
		recherche_diplome = recherche_diplome.replaceAll("é", "?");
		recherche_diplome = recherche_diplome.replaceAll("è", "?");
		recherche_diplome = recherche_diplome.replaceAll("ê", "?");
		recherche_diplome = recherche_diplome.replaceAll("â", "?");
		recherche_diplome = recherche_diplome.replaceAll("ô", "?");
		recherche_diplome = recherche_diplome.replaceAll("û", "?");
		recherche_diplome = recherche_diplome.replaceAll("î", "?");
		recherche_diplome = recherche_diplome.replaceAll("ç", "?");
		recherche_diplome = recherche_diplome.replaceAll("0", "'");
		recherche_diplome = recherche_diplome.replaceAll("7", "(");
		recherche_diplome = recherche_diplome.replaceAll("8", ")");
		recherche_diplome = recherche_diplome.replaceAll("9", ",");
		recherche_type = recherche_type.toLowerCase();
		System.out.println("recherche diplome "+recherche_diplome + " niveau "+ recherche_type);
    }
	
	public void etude() throws MalformedURLException, IOException, XPatherException
	{	
		if(recherche_type != "")
		{
			if(recherche_type.contains("dut"))
			{
				parser_liste(LiensDUT);
				recherche_liens();
			}
			else if(recherche_type.contains("licence")) 
			{
				parser_liste(LiensLicence);
				recherche_liens();
			}
			else if(recherche_type.contains("master"))
			{
				parser_liste(LiensMaster1);
				recherche_liens();
				parser_liste(LiensMaster2);
				recherche_liens();
				parser_liste(LiensMaster3);
				recherche_liens();
				parser_liste(LiensMaster4);
				recherche_liens();
				parser_liste(LiensMaster5);
				recherche_liens();
			}
			else if(recherche_type.contains("doctorat"))
			{
				parser_liste(LiensDoctorat);
				recherche_liens();
			}
			else if(recherche_type.contains("medecine")) 
			{
				parser_liste(LiensMedecine);
				recherche_liens();
			}
			else if(recherche_type.contains("ingenieur"))
			{
				parser_liste(LiensIngenieur);
				recherche_liens();
			}
			else if(recherche_type.contains("prepa")) 
			{
				parser_liste(LiensPrepa);
				recherche_liens();
			}
			else if(recherche_type.contains("daeu"))
			{
				parser_liste(LiensDAEU);
				recherche_liens();
			}
			else if(recherche_type.contains("du")) 
			{
				parser_liste(LiensDU);
				recherche_liens();
			}
			else if(recherche_type.contains("diu"))
			{
				parser_liste(LiensDIU);
				recherche_liens();
			}
			else if(recherche_type.contains("certificat")) 
			{
				parser_liste(LiensCertificat);
				recherche_liens();
			}
			else
			{
				
			}
			//recherche_liens();
			//parser_final(lien_final);
		}
    } 
		
	public static void parser_liste (String lien) throws MalformedURLException, IOException, XPatherException
	{		
		FileWriter writer = null;
		writer = new FileWriter("resultats", false);
		String ajout = new String();
			
		CleanerProperties props = new CleanerProperties();
		props.setAdvancedXmlEscape(true);
		props.setTransResCharsToNCR(true);
		props.setTranslateSpecialEntities(true);
		props.setTransSpecialEntitiesToNCR(true);
		props.setRecognizeUnicodeChars(true);
		props.setCharset("UTF-8");
		
		HtmlCleaner cleaner = new HtmlCleaner(props);
			
		TagNode node = cleaner.clean(new URL(lien));
		//TagNode tagNode = new HtmlCleaner(props).clean(new URL(lien), "UTF-8");
		//System.out.println("Title: " + ((TagNode)(node.evaluateXPath("//title")[0])).getText());
		for (Object o : node.evaluateXPath("//div[@id='contenu_deco']//li/a")) 
		{
			String dUrl = ((TagNode)(o)).getAttributeByName("href");
			//System.out.println("LI: " + ((org.apache.commons.lang3.StringEscapeUtils.unescapeHtml4(((TagNode)(o)).getText().toString()))));
			//System.out.println("href: "+dUrl);
			ajout = "LI: " + org.apache.commons.lang3.StringEscapeUtils.unescapeHtml4(((TagNode)(o)).getText().toString())+"\n";
			ajout += dUrl+"\n";
			writer.write(ajout, 0, ajout.length());
		}
		//System.out.println();
		writer.close();
	}
		
	public static void recherche_liens() throws IOException, XPatherException 
	{
		String ligne = "";
		String fichier = "resultats";
		BufferedReader ficTexte;
		try 
		{
			ficTexte = new BufferedReader(new FileReader(new File(fichier)));
			if (ficTexte == null) 
			{
				throw new FileNotFoundException("Fichier non trouvé: "+ fichier);
			}
			do 
			{
				ligne = ficTexte.readLine();
				if (ligne != null && ligne.contains(recherche_diplome)) 
				{
					//System.out.println("trouvé !!!!!!!!!! "+ ligne);
					ligne = ficTexte.readLine();
					//System.out.println("le lien !!!!!!!!!! "+ ligne);
					lien_final = ligne;
					parser_final(lien_final);
					break;
				}
				else if(ligne == null)
				{
					break;
				}
				else
				{
					
				}
			} 
			while (ficTexte != null);
			ficTexte.close();
			//System.out.println("\n");
		} 
		catch (FileNotFoundException e) 
		{
			System.out.println(e.getMessage());
		}
		catch (IOException e) 
		{
			System.out.println(e.getMessage());
		}
	}
		
	public static void parser_final (String lien) throws MalformedURLException, IOException, XPatherException
	{				
		FileWriter writer = null;
		writer = new FileWriter("resultats_final", false);
			
		CleanerProperties props = new CleanerProperties();
		props.setTranslateSpecialEntities(true);
		props.setTransResCharsToNCR(true);
		props.setTransSpecialEntitiesToNCR(true);
		props.setOmitComments(true);
			
		HtmlCleaner cleaner = new HtmlCleaner(props);
			
		int i=0;
		TagNode node = cleaner.clean(new URL(lien));
		
		for (Object o : node.evaluateXPath("//div[@id='infos_generales']/table/tbody/tr")) 
		{
			for(Object temp: ((TagNode)(o)).getAllChildren())
			{
				if(temp.toString().contains("th"))
				{
					for(Object temp2: ((TagNode)(temp)).getAllChildren())
					{
						info_principal[i][0] = org.apache.commons.lang3.StringEscapeUtils.unescapeHtml4(temp2.toString()); 
					}
				}
				if(temp.toString().contains("td"))
				{
					for(Object temp2: ((TagNode)(temp)).getAllChildren())
					{
						info_principal[i][1] = org.apache.commons.lang3.StringEscapeUtils.unescapeHtml4(temp2.toString()); 
						i++;
					}
				}
			}
		}
		
		
		for (Object o : node.evaluateXPath("//div[@id='infos_generales']/table/tbody/tr/td/a")) 
		{
			for(Object temp: ((TagNode)(o)).getAllChildren())
			{
				if(temp.toString().length()>=5)
				{
					int j=0;
					for(j=0;j<20;j++)
					{
						if(info_principal[j][0]!=null && info_principal[j][0].contains("Composante(s)"))
						{
							info_principal[j][1] = org.apache.commons.lang3.StringEscapeUtils.unescapeHtml4(temp.toString());	
						}
					}
				}
			}
		}
		
		i=0;
		for (Object o : node.evaluateXPath("//div[@id='contenu_onglet_presentation']/h3")) 
		{
			for(Object temp: ((TagNode)(o)).getAllChildren())
			{
				if(temp.toString().length()>=5)
				{
					description[i][0] = org.apache.commons.lang3.StringEscapeUtils.unescapeHtml4(temp.toString());
				}
			}
		}
		
		for (Object o : node.evaluateXPath("//div[@id='contenu_onglet_presentation']/div")) 
		{
			for(Object temp: ((TagNode)(o)).getAllChildren())
			{
				if(temp.toString().contains("h3"))
				{
					for(Object temp2: ((TagNode)(temp)).getAllChildren())
					{
						description[i][0] = org.apache.commons.lang3.StringEscapeUtils.unescapeHtml4(temp2.toString());
					}
				}
				else if(temp.toString().length()>=10)
				{
					description[i][1] = org.apache.commons.lang3.StringEscapeUtils.unescapeHtml4(temp.toString());
					i++;
				}
			}
		}
		
		i=0;
		int i_temp = 0;
		String contenu = new String();
		for (Object o : node.evaluateXPath("//div[@id='contenu_onglet_contact']/div")) 
		{
			for(Object temp: ((TagNode)(o)).getAllChildren())
			{
				if(temp.toString().contains("h3"))
				{
					for(Object temp2: ((TagNode)(temp)).getAllChildren())
					{
						//System.out.println("contenu 7 "+temp2.toString());
					}
				}
				if(temp.toString().contains("p"))
				{
					for(Object temp2: ((TagNode)(temp)).getAllChildren())
					{
						if(temp2.toString().contains("strong"))
						{
							for(Object temp3: ((TagNode)(temp2)).getAllChildren())
							{
								if(temp3.toString().contains("br"))
								{
									
								}
								else
								{
									i_temp = i;
									contenu = temp3.toString();
								}
							}
						}
						else if(temp2.toString().length()>=3)
						{
							if(i == i_temp)
							{
								coordonnees[i][0]=org.apache.commons.lang3.StringEscapeUtils.unescapeHtml4(contenu);
								coordonnees[i][1]=org.apache.commons.lang3.StringEscapeUtils.unescapeHtml4(temp2.toString());
								i++;
								i_temp = i;
							}
						}
						else if(temp2.toString().contains("a"))
						{

						}
					}
				}
			}
		} 
		
		/*i=0;
		System.out.println("DESCRIPTION");
		for(i=0;i<20;i++)
		{
			if(description[i][0]!=null)
			{
				System.out.println("0 "+description[i][0]);
				System.out.println("1 "+description[i][1]);
			}
		}
		System.out.println();
		
		i=0;
		System.out.println("INFORMATION PRINCIPAL");
		for(i=0;i<20;i++)
		{
			if(info_principal[i][0]!=null)
			{
				System.out.println("0 "+info_principal[i][0]);
				System.out.println("1 "+info_principal[i][1]);
			}
		}
		System.out.println();
		
		i=0;
		System.out.println("COORDONNEES");
		for(i=0;i<20;i++)
		{
			if(coordonnées[i][0]!=null)
			{
				System.out.println("0 "+coordonnées[i][0]);
				System.out.println("1 "+coordonnées[i][1]);
			}
		}
		
		System.out.println();*/
		writer.close();
	}
	
	public JPanel resultat() throws MalformedURLException, IOException, XPatherException 
    { 
		
		JPanel gauche =  new JPanel();
		JPanel droite =  new JPanel();
		
		Font police_base = new Font("Arial", Font.BOLD, 10);
		Font police_titre = new Font("Arial", Font.BOLD, 20);
		
		JTextArea titre = new JTextArea();
		titre.setFont(police_titre);
		titre.setPreferredSize(new Dimension(500, 50));
		titre.setForeground(Color.black); 
		titre.setText(recherche_diplome_affichage);
		titre.setLineWrap(true);
		titre.setWrapStyleWord(true); 
		
		JTextArea panel_info_principal = new JTextArea();
		panel_info_principal.setFont(police_base);
		panel_info_principal.setPreferredSize(new Dimension(500, 150));
		panel_info_principal.setForeground(Color.black); 
		panel_info_principal.setLineWrap(true);
		panel_info_principal.setWrapStyleWord(true); 
		
		int i=0;
		for(i=0;i<20;i++)
		{ 
			if(info_principal[i][0]!=null)
			{
				panel_info_principal.setText(panel_info_principal.getText()+"\n"+info_principal[i][0]+" : "+info_principal[i][1]);
			}
		}
		
		JLabel titre_description = new JLabel();
		titre_description.setFont(police_titre);
		titre_description.setPreferredSize(new Dimension(500, 30));
		titre_description.setForeground(Color.black); 
		titre_description.setText("Description");
		
		JTextArea panel_description = new JTextArea();
		panel_description.setFont(police_base);
		panel_description.setPreferredSize(new Dimension(500, 120));
		panel_description.setForeground(Color.black); 
		panel_description.setLineWrap(true);
		panel_description.setWrapStyleWord(true); 
		
		/*i=0;
		for(i=0;i<20;i++)
		{ 
			if(description[i][0]!=null)
			{
				panel_description.setText(panel_description.getText()+"\n"+description[i][0]+" "+description[i][1]);
			}
		}*/
		
		panel_description.setText(panel_description.getText()+"\n"+description[0][0]+" "+description[0][1]);
		
		JLabel titre_coordonnees = new JLabel();
		titre_coordonnees.setFont(police_titre);
		titre_coordonnees.setPreferredSize(new Dimension(500, 30));
		titre_coordonnees.setForeground(Color.black); 
		titre_coordonnees.setText("Coordonnées");
		
		JTextArea panel_coordonnees = new JTextArea();
		panel_coordonnees.setFont(police_base);
		panel_coordonnees.setPreferredSize(new Dimension(500, 200));
		panel_coordonnees.setForeground(Color.black); 
		panel_coordonnees.setLineWrap(true);
		panel_coordonnees.setWrapStyleWord(true); 
		
		i=0;
		for(i=0;i<20;i++)
		{ 
			if(coordonnees[i][0]!=null)
			{
				panel_coordonnees.setText(panel_coordonnees.getText()+"\n"+coordonnees[i][0]+" "+coordonnees[i][1]);
			}
		}
		
		gauche.setBackground(Color.white);
    	gauche.setPreferredSize(new Dimension(500, 630));
    	gauche.add(titre,BorderLayout.NORTH);
    	gauche.add(panel_info_principal,BorderLayout.NORTH);
    	gauche.add(titre_description,BorderLayout.NORTH);
    	gauche.add(panel_description,BorderLayout.NORTH);
    	gauche.add(titre_coordonnees,BorderLayout.NORTH);
    	gauche.add(panel_coordonnees,BorderLayout.NORTH);
		
    	JPanel panel_uvsq = new JPanel();
		panel_uvsq.setBackground(Color.white);
		panel_uvsq.setPreferredSize(new Dimension(530, 200));
		
		java.net.URL where;
		try 
		{
			where = new URL("http://www.uvsq.fr/images/logo.png");
			ImageIcon logo = new ImageIcon(where);
			ImagePanel logo_uvsq = new ImagePanel(logo.getImage());
			panel_uvsq.add(logo_uvsq,BorderLayout.NORTH);
		} catch (MalformedURLException e) 
		{
			e.printStackTrace();
		}
		
		ParserUVSQ p = new ParserUVSQ();
		JTextArea info_UVSQ = new JTextArea();
		info_UVSQ.setFont(police_base);
		info_UVSQ.setPreferredSize(new Dimension(500, 200));
		info_UVSQ.setForeground(Color.black); 
		info_UVSQ.setLineWrap(true);
		info_UVSQ.setWrapStyleWord(true);
		info_UVSQ.setText(p.getInfos());
		panel_uvsq.add(info_UVSQ,BorderLayout.NORTH);
		
		JPanel panel_composante = new JPanel();
		panel_composante.setBackground(Color.white);
		panel_composante.setPreferredSize(new Dimension(530, 200));
		
		ParserComposante c = new ParserComposante(recherche_composante);
		c.etude();
		java.net.URL where2;
		try 
		{
			where2 = new URL(c.getLien_logo());
			ImageIcon logo2 = new ImageIcon(where2);
			ImagePanel logo_composante = new ImagePanel(logo2.getImage());
			panel_composante.add(logo_composante,BorderLayout.NORTH);
		} catch (MalformedURLException e) 
		{
			e.printStackTrace();
		}
		JTextArea info_composante = new JTextArea();
		info_composante.setFont(police_base);
		info_composante.setPreferredSize(new Dimension(500, 200));
		info_composante.setForeground(Color.black); 
		info_composante.setLineWrap(true);
		info_composante.setWrapStyleWord(true);
		i=0;
		for(i=0;i<20;i++)
		{ 
			if(ParserComposante.getDescription()[i][0]!=null)
			{
				info_composante.setText(info_composante.getText()+"\n"+ParserComposante.getDescription()[i][0]+" "+c.getDescription()[i][1]);
			}
		}
		panel_composante.add(info_composante,BorderLayout.NORTH);
		
		RechercheRDF temp = new RechercheRDF(recherche_composante);
		/*diplome1[0] = new String();
		diplome1[0] = temp.getDiplome(recherche_composante).get(0)[0];
		diplome1[1] = new String();
		diplome1[1] = temp.getDiplome(recherche_composante).get(0)[1];*/
		diplome1 = temp.getDiplome(recherche_composante).get(0);
		//System.out.println("DIPLOME1 0 "+diplome1[0]);
		//System.out.println("DIPLOME1 1 "+diplome1[1]);
		//diplome1 = "Master 2 professionnel Ingénierie de la Statistique";
		ParserDiplome c1 = new ParserDiplome(diplome1[1],diplome1[0],recherche_composante);
		JPanel panel_diplome1 = new JPanel();
		panel_diplome1.setBackground(Color.white);
		panel_diplome1.setPreferredSize(new Dimension(170, 160));
		JTextArea nom_diplome1= new JTextArea(c1.getRecherche_diplome_affichage());
		nom_diplome1.setLineWrap(true);
		nom_diplome1.setWrapStyleWord(true); 
		JButton button_diplome1 = new JButton();
		button_diplome1.setPreferredSize(new Dimension(150, 30));
		button_diplome1.setText("ACCEDER");
		button_diplome1.addActionListener(new diplome1Listener());
		panel_diplome1.add(nom_diplome1);
		panel_diplome1.add(button_diplome1);
		
		/*diplome2[0] = new String();
		diplome2[0] = temp.getDiplome(recherche_composante).get(1)[0];
		diplome2[1] = new String();
		diplome2[1] = temp.getDiplome(recherche_composante).get(1)[1];*/
		diplome2 = temp.getDiplome(recherche_composante).get(1);
		//System.out.println("DIPLOME2 0 "+diplome2[0]);
		//System.out.println("DIPLOME2 1 "+diplome2[1]);
		//diplome2 = "Master 2 recherche et professionnel Modélisation et Simulation";
		ParserDiplome c2 = new ParserDiplome(diplome2[1],diplome2[0],recherche_composante);
		JPanel panel_diplome2 = new JPanel();
		panel_diplome2.setBackground(Color.white);
		panel_diplome2.setPreferredSize(new Dimension(170, 160));
		JTextArea nom_diplome2= new JTextArea(c2.getRecherche_diplome_affichage());
		nom_diplome2.setLineWrap(true);
		nom_diplome2.setWrapStyleWord(true);
		JButton button_diplome2 = new JButton();
		button_diplome2.setPreferredSize(new Dimension(150, 30));
		button_diplome2.setText("ACCEDER");
		button_diplome2.addActionListener(new diplome2Listener());
		panel_diplome2.add(nom_diplome2);
		panel_diplome2.add(button_diplome2);
		
		/*diplome3[0] = new String();
		diplome3[0] = temp.getDiplome(recherche_composante).get(2)[0];
		diplome3[1] = new String();
		diplome3[1] = temp.getDiplome(recherche_composante).get(2)[1];*/
		diplome3 = temp.getDiplome(recherche_composante).get(2);
		//System.out.println("DIPLOME3 0 "+diplome3[0]);
		//System.out.println("DIPLOME3 1 "+diplome3[1]);
		//diplome3 = "Master 2 recherche et professionnel Algèbre Appliquée à la Cryptographie et au Calcul Formel";
		ParserDiplome c3 = new ParserDiplome(diplome3[1],diplome3[0],recherche_composante);
		JPanel panel_diplome3 = new JPanel();
		panel_diplome3.setBackground(Color.white);
		panel_diplome3.setPreferredSize(new Dimension(170, 160));
		JTextArea nom_diplome3= new JTextArea(c3.getRecherche_diplome_affichage());
		nom_diplome3.setLineWrap(true);
		nom_diplome3.setWrapStyleWord(true);
		JButton button_diplome3 = new JButton();
		button_diplome3.setPreferredSize(new Dimension(150, 30));
		button_diplome3.setText("ACCEDER");
		button_diplome3.addActionListener(new diplome3Listener());
		panel_diplome3.add(nom_diplome3);
		panel_diplome3.add(button_diplome3);
		
		JPanel panel_prof = new JPanel();
		//panel_prof.setBackground(Color.white);
		panel_prof.setPreferredSize(new Dimension(530, 160));
		panel_prof.add(panel_diplome1,BorderLayout.EAST);
		panel_prof.add(panel_diplome2,BorderLayout.EAST);
		panel_prof.add(panel_diplome3,BorderLayout.EAST);
    	
    	//droite.setBackground(Color.white);
    	droite.setPreferredSize(new Dimension(530, 640));
    	droite.add(panel_uvsq,BorderLayout.NORTH);
    	droite.add(panel_composante,BorderLayout.NORTH);
    	droite.add(panel_prof,BorderLayout.NORTH);
    	
		//resultat.setBackground(Color.white);
    	resultat.setPreferredSize(new Dimension(1050, 650));
    	resultat.add(gauche,BorderLayout.EAST);
    	resultat.add(droite,BorderLayout.WEST);
		return resultat;
    }
	
	class diplome1Listener implements ActionListener
    {
		@Override
        public void actionPerformed(ActionEvent e) 
        {
			try {
				resultat.removeAll();
				ParserDiplome c1;
				System.out.println("NOUVELLE PAGE "+diplome1);
				c1 = new ParserDiplome(diplome1[1],diplome1[0],recherche_composante);
				c1.etude();
				JPanel panelNewDiplome = new JPanel();
				panelNewDiplome = c1.resultat();
				BorderLayout test = new BorderLayout();
				resultat.add(panelNewDiplome,test);
				resultat.validate();
				resultat.repaint();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (XPatherException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
        }
    }
	class diplome2Listener implements ActionListener
    {
		@Override
        public void actionPerformed(ActionEvent e) 
        {
			try {
				resultat.removeAll();
				ParserDiplome c2;
				System.out.println("NOUVELLE PAGE "+diplome2);
				c2 = new ParserDiplome(diplome2[1],diplome2[0],recherche_composante);
				c2.etude();
				JPanel panelNewDiplome = new JPanel();
				panelNewDiplome = c2.resultat();
				BorderLayout test = new BorderLayout();
				resultat.add(panelNewDiplome,test);
				resultat.validate();
				resultat.repaint();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (XPatherException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
        }
    }
	class diplome3Listener implements ActionListener
    {
		 @Override
         public void actionPerformed(ActionEvent e) 
         {
			try {
				resultat.removeAll();
				ParserDiplome c3;
				System.out.println("NOUVELLE PAGE "+diplome3);
				c3 = new ParserDiplome(diplome3[1],diplome3[0],recherche_composante);
				c3.etude();
				JPanel panelNewDiplome = new JPanel();
				panelNewDiplome = c3.resultat();
				BorderLayout test = new BorderLayout();
				resultat.add(panelNewDiplome,test);
				resultat.validate();
				resultat.repaint();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (XPatherException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
         }
    }

	public static String getRecherche_type() {
		return recherche_type;
	}

	public static void setRecherche_type(String recherche_type) {
		ParserDiplome.recherche_type = recherche_type;
	}

	public static String getRecherche_diplome() {
		return recherche_diplome;
	}

	public static void setRecherche_diplome(String recherche_diplome) {
		ParserDiplome.recherche_diplome = recherche_diplome;
	}

	public static String getRecherche_composante() {
		return recherche_composante;
	}

	public static void setRecherche_composante(String recherche_composante) {
		ParserDiplome.recherche_composante = recherche_composante;
	}

	public static String getRecherche_diplome_affichage() {
		return recherche_diplome_affichage;
	}

	public static void setRecherche_diplome_affichage(
			String recherche_diplome_affichage) {
		ParserDiplome.recherche_diplome_affichage = recherche_diplome_affichage;
	}
	
}
