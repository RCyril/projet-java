package ProjetJavaRDF.RDF;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.htmlcleaner.XPatherException;

public class ParserComposante 
{
	private JPanel resultat = new JPanel();
	
	private static String[][] info_principal = new String[20][2];	
	private static String[][] description = new String[20][2];
	private static String[][] coordonnees = new String[20][2];
	
	private static String composante_rechercher = new String();
	private static String composante_rechercher_affichage = new String();
	private static String composante1= new String();
	private static String composante2= new String();
	private static String composante3= new String();
	private static String lien_final= new String();
	
	private static String lien_site_spe= new String();
	private static String lien_logo= new String();
	private static String lien_image= new String();
	private static String LienComposantes = "http://www.uvsq.fr/les-composantes-234132.kjsp?RH=1177070307923&RF=1188915694376";

	public ParserComposante(String rech)
    {
		info_principal = new String[20][2];
		description = new String[20][2];
		coordonnees = new String[20][2];
		composante_rechercher_affichage = rech;
		composante_rechercher_affichage = composante_rechercher_affichage.replaceAll("_", " ");
		composante_rechercher_affichage = composante_rechercher_affichage.replaceAll("0", "'");
		composante_rechercher_affichage = composante_rechercher_affichage.replaceAll("7", "(");
		composante_rechercher_affichage = composante_rechercher_affichage.replaceAll("8", ")");
		composante_rechercher_affichage = composante_rechercher_affichage.replaceAll("9", ",");
		composante_rechercher = rech;
		composante_rechercher = composante_rechercher.replaceAll("_", " ");
		composante_rechercher = composante_rechercher.replaceAll("�", "?");
		composante_rechercher = composante_rechercher.replaceAll("�", "?");
		composante_rechercher = composante_rechercher.replaceAll("�", "?");
		composante_rechercher = composante_rechercher.replaceAll("�", "?");
		composante_rechercher = composante_rechercher.replaceAll("�", "?");
		composante_rechercher = composante_rechercher.replaceAll("�", "?");
		composante_rechercher = composante_rechercher.replaceAll("�", "?");
		composante_rechercher = composante_rechercher.replaceAll("�", "?");
		composante_rechercher = composante_rechercher.replaceAll("�", "?");
		composante_rechercher = composante_rechercher.replaceAll("0", "'");
		composante_rechercher = composante_rechercher.replaceAll("7", "(");
		composante_rechercher = composante_rechercher.replaceAll("8", ")");
		composante_rechercher = composante_rechercher.replaceAll("9", ",");
		//System.out.println("recherche composante "+composante_rechercher);
    }
	
	public void etude() throws MalformedURLException, IOException, XPatherException
	{
		if(composante_rechercher != "")
		{
			parser_liste(LienComposantes);
			recherche_liens();
			parser_final(lien_final);
			parser_site_spe(lien_site_spe);
		}
    } 
	
	public static void parser_liste (String lien) throws MalformedURLException, IOException, XPatherException
	{		
		FileWriter writer = null;
		writer = new FileWriter("resultats", false);
		String ajout = new String();
			
		CleanerProperties props = new CleanerProperties();
		props.setAdvancedXmlEscape(true);
		props.setTransResCharsToNCR(true);
		props.setTranslateSpecialEntities(true);
		props.setTransSpecialEntitiesToNCR(true);
		
		HtmlCleaner cleaner = new HtmlCleaner(props);
			
		TagNode node = cleaner.clean(new URL(lien));
		//System.out.println("Title: " + ((TagNode)(node.evaluateXPath("//title")[0])).getText());
		for (Object o : node.evaluateXPath("//ul[@id='acces_1']//li/a")) 
		{
			String dUrl = ((TagNode)(o)).getAttributeByName("href");
			//System.out.println("LI: " + org.apache.commons.lang3.StringEscapeUtils.unescapeHtml4(((TagNode)(o)).getText().toString()));
			//System.out.println("href: "+dUrl);
			ajout = "LI: " + org.apache.commons.lang3.StringEscapeUtils.unescapeHtml4(((TagNode)(o)).getText().toString())+"\n";
			ajout += dUrl+"\n";
			writer.write(ajout, 0, ajout.length());
		}
		System.out.println();
		writer.close();
	}
	
	public static void recherche_liens() throws IOException 
	{
		String ligne = "";
		String fichier = "resultats";
		BufferedReader ficTexte;
		try 
		{
			ficTexte = new BufferedReader(new FileReader(new File(fichier)));
			if (ficTexte == null) 
			{
				throw new FileNotFoundException("Fichier non trouv�: "+ fichier);
			}
			do 
			{
				ligne = ficTexte.readLine();
				if (ligne != null && ligne.contains(composante_rechercher)) 
				{
					//System.out.println("trouv� !!!!!!!!!! "+ ligne);
					ligne = ficTexte.readLine();
					//System.out.println("le lien !!!!!!!!!! "+ ligne);
					lien_final = ligne;
					break;
				}
			} 
			while (ficTexte != null);
			ficTexte.close();
			System.out.println("\n");
		} 
		catch (FileNotFoundException e) 
		{
			System.out.println(e.getMessage());
		}
		catch (IOException e) 
		{
			System.out.println(e.getMessage());
		}
	}
	
	public static void parser_site_spe(String lien) throws MalformedURLException, IOException, XPatherException
	{
		CleanerProperties props = new CleanerProperties();
		props.setTranslateSpecialEntities(true);
		props.setTransResCharsToNCR(true);
		props.setTransSpecialEntitiesToNCR(true);
		props.setOmitComments(true);
			
		HtmlCleaner cleaner = new HtmlCleaner(props);

		TagNode node = cleaner.clean(new URL(lien));
		
		for (Object o : node.evaluateXPath("//div[@id='retour_accueil']/a/img")) 
		{
			//System.out.println(((TagNode)(o)).getAllChildren());
			lien_logo = ((TagNode)(o)).getAttributeByName("src");
			//System.out.println("lien_logo "+lien_logo);
		}
		
		String temp2[] = new String[10];
		for (Object o : node.evaluateXPath("//style")) 
		{
			//System.out.println(((TagNode)(o)).getAllChildren());
			for(Object temp: ((TagNode)(o)).getAllChildren())
			{
				if(temp.toString().contains("}"))
				{
					temp2 = temp.toString().split("(background-image : url)");
				}
			}
		}
		
		int i=0;
		for(String s :temp2)
		{
			//System.out.println("string "+s);
			lien_image = s.substring(1, s.length()-10);
			//System.out.println("lien image "+lien_image);
		}
	}
	
	public static void parser_final (String lien) throws MalformedURLException, IOException, XPatherException
	{				
		FileWriter writer = null;
		writer = new FileWriter("resultats_final", false);
			
		CleanerProperties props = new CleanerProperties();
		props.setTranslateSpecialEntities(true);
		props.setTransResCharsToNCR(true);
		props.setTransSpecialEntitiesToNCR(true);
		props.setOmitComments(true);
			
		HtmlCleaner cleaner = new HtmlCleaner(props);
			
		int i=0;
		TagNode node = cleaner.clean(new URL(lien));
		
		for (Object o : node.evaluateXPath("//div[@class='encadre_fiche firstencadre']/div/div/a")) 
		{
			lien_site_spe = ((TagNode)(o)).getAttributeByName("href");
			//System.out.println("lien spe "+lien_site_spe);
		}
		
		for (Object o : node.evaluateXPath("//div[@id='infos_generales']/table/tbody/tr")) 
		{
			for(Object temp: ((TagNode)(o)).getAllChildren())
			{
				if(temp.toString().contains("th"))
				{
					for(Object temp2: ((TagNode)(temp)).getAllChildren())
					{
						info_principal[i][0] = org.apache.commons.lang3.StringEscapeUtils.unescapeHtml4(temp2.toString()); 
					}
				}
				if(temp.toString().contains("td"))
				{
					for(Object temp2: ((TagNode)(temp)).getAllChildren())
					{
						info_principal[i][1] = org.apache.commons.lang3.StringEscapeUtils.unescapeHtml4(temp2.toString()); 
						i++;
					}
				}
			}
		}
		
		i=0;
		for (Object o : node.evaluateXPath("//div[@class='element_deco'][2]")) 
		{
			//System.out.println("description "+((TagNode)(o)).getAllChildren());
			for(Object temp: ((TagNode)(o)).getAllChildren())
			{
				if(temp.toString().contains("strong"))
				{
					for(Object temp2: ((TagNode)(temp)).getAllChildren())
					{
						description[i][0] =org.apache.commons.lang3.StringEscapeUtils.unescapeHtml4(temp2.toString());
					}
				}
				else if(temp.toString().equals("div"))
				{
					for(Object temp2: ((TagNode)(temp)).getAllChildren())
					{
						if(temp2.toString().equals("font"))
						{
							for(Object temp3: ((TagNode)(temp2)).getAllChildren())
							{
								if(temp3.toString().equals("strong"))
								{
									for(Object temp4: ((TagNode)(temp3)).getAllChildren())
									{
										//System.out.println("description strong "+temp4.toString());
										description[i][0] =org.apache.commons.lang3.StringEscapeUtils.unescapeHtml4(temp2.toString());
										i++;
									}
								}
								else if(temp3.toString().length()>=5)
								{
									//System.out.println("description font "+temp3.toString());
									description[i][0] =org.apache.commons.lang3.StringEscapeUtils.unescapeHtml4(temp2.toString());
									i++;
								}
							}
						}
						else if(temp2.toString().length()>=5)
						{
							//System.out.println("description div "+temp2.toString());
							description[i][0] =org.apache.commons.lang3.StringEscapeUtils.unescapeHtml4(temp2.toString());
							i++;
						}
					}
				}
				else if(temp.toString().equals("p"))
				{
					for(Object temp2: ((TagNode)(temp)).getAllChildren())
					{
						if(temp2.toString().equals("strong"))
						{
							for(Object temp3: ((TagNode)(temp2)).getAllChildren())
							{
								//System.out.println("description 3 "+temp3.toString());
								break;
							}
						}
						else if(temp2.toString().length()>=5)
						{
							//System.out.println("description i "+i+" "+temp2.toString());
							description[i][0] =org.apache.commons.lang3.StringEscapeUtils.unescapeHtml4(temp2.toString());
							i++;
							break;
						}
					}
				}
				else if(temp.toString().length()>=5)
				{
					description[i][1] = org.apache.commons.lang3.StringEscapeUtils.unescapeHtml4(temp.toString());
					break;
				}
			}
		}
		
		i=0;
		int ok=0;
		for (Object o : node.evaluateXPath("//div[@class='encadre_fiche']")) 
		{
			for(Object temp: ((TagNode)(o)).getAllChildren())
			{
				if(ok == 1)
				{
					//System.out.println("coordonnees "+((TagNode)(o)).getAllChildren());
					if(temp.toString().contains("div"))
					{
						for(Object temp1: ((TagNode)(temp)).getAllChildren())
						{
							//System.out.println("contact div "+temp1.toString());
							if(temp1.toString().contains("strong"))
							{
								for(Object temp2: ((TagNode)(temp1)).getAllChildren())
								{
									if(temp2.toString().length()>=6)
									{
										//System.out.println("contact div strong "+temp2.toString());
										coordonnees[i][1]= org.apache.commons.lang3.StringEscapeUtils.unescapeHtml4(temp2.toString());
										i++;
									}
								}
							}
							else if(temp1.toString().length()>=5)
							{
								//System.out.println("contact div "+temp1.toString());
								coordonnees[i][1]= org.apache.commons.lang3.StringEscapeUtils.unescapeHtml4(temp1.toString());
								i++;
							}
							else if(temp1.toString().contains("p"))
							{
								for(Object temp2: ((TagNode)(temp1)).getAllChildren())
								{
									if(temp2.toString().contains("em"))
									{
										for(Object temp3: ((TagNode)(temp2)).getAllChildren())
										{
											if(temp3.toString().contains("br"))
											{
												
											}
											else
											{
												//System.out.println("em "+temp3.toString());
												coordonnees[i][0]= org.apache.commons.lang3.StringEscapeUtils.unescapeHtml4(temp3.toString());
												if(coordonnees[i][1]==null)
												{
													coordonnees[i][1] = coordonnees[i-1][1];
												}	
												i++;
											}
										}
									}
									else if(temp2.toString().contains("b"))
									{
										for(Object temp3: ((TagNode)(temp2)).getAllChildren())
										{
											if(temp3.toString().contains("br"))
											{
												
											}
											else
											{
												//System.out.println("strong "+temp3.toString());
												coordonnees[i][1] = org.apache.commons.lang3.StringEscapeUtils.unescapeHtml4(temp3.toString());
												i++;
											}
										}
									}
									else if(temp2.toString().contains("strong"))
									{
										for(Object temp3: ((TagNode)(temp2)).getAllChildren())
										{
											if(temp3.toString().contains("br"))
											{
												
											}
											else
											{
												//System.out.println("strong "+temp3.toString());
												coordonnees[i][1] = org.apache.commons.lang3.StringEscapeUtils.unescapeHtml4(temp3.toString());
												i++;
											}
										}
									}
									//System.out.println("p "+temp2.toString());
								}
							}
							else
							{
								//System.out.println("pas de strong");
							}
						}
						ok = 0;
					}
				}
				else if(temp.toString().contains("strong"))
				{
					for(Object temp1: ((TagNode)(temp)).getAllChildren())
					{
						//System.out.println("strong "+temp1.toString());
						coordonnees[i][1] = org.apache.commons.lang3.StringEscapeUtils.unescapeHtml4(temp1.toString());
						i++;
					}
				}
				else if(temp.toString().contains("h3"))
				{
					for(Object temp1: ((TagNode)(temp)).getAllChildren())
					{
						if(temp1.toString().contains("Contacts"))
						{
							ok=1;
						}
					}
				}
				else if(temp.toString().length()>=6)
				{
					//System.out.println("base "+temp.toString());
					coordonnees[i][1] = org.apache.commons.lang3.StringEscapeUtils.unescapeHtml4(temp.toString());
					i++;
				}
			}
		}
		
		/*i=0;
		System.out.println("DESCRIPTION");
		for(i=0;i<20;i++)
		{
			if(description[i][0]!=null)
			{
				System.out.println("0 "+description[i][0]);
				System.out.println("1 "+description[i][1]);
			}
		}
		System.out.println();
		
		i=0;
		System.out.println("INFORMATION PRINCIPAL");
		for(i=0;i<20;i++)
		{
			if(info_principal[i][0]!=null)
			{
				System.out.println("0 "+info_principal[i][0]);
				System.out.println("1 "+info_principal[i][1]);
			}
		}
		System.out.println();
		
		i=0;
		System.out.println("COORDONNEES");
		for(i=0;i<20;i++)
		{
			System.out.println("0 "+coordonn�es[i][0]);
			System.out.println("1 "+coordonn�es[i][1]);
		}*/
		
		System.out.println();
		writer.close();
	}
	
	public JPanel resultat() throws MalformedURLException, IOException, XPatherException 
    { 
		JPanel gauche =  new JPanel();
		JPanel droite =  new JPanel();
		
		Font police_base = new Font("Arial", Font.BOLD, 10);
		Font police_titre = new Font("Arial", Font.BOLD, 20);
		
		JTextArea titre = new JTextArea();
		titre.setFont(police_titre);
		titre.setPreferredSize(new Dimension(500, 50));
		titre.setForeground(Color.black); 
		titre.setText(composante_rechercher_affichage);
		titre.setLineWrap(true);
		titre.setWrapStyleWord(true); 
		
		JPanel panel_info_principal = new JPanel();
		panel_info_principal.setBackground(Color.white);
		panel_info_principal.setFont(police_base);
		panel_info_principal.setPreferredSize(new Dimension(500, 250));
		panel_info_principal.setForeground(Color.black); 
		
		JTextArea panel_info = new JTextArea();
		panel_info.setFont(police_base);
		panel_info.setPreferredSize(new Dimension(200, 50));
		panel_info.setForeground(Color.black); 
		panel_info.setLineWrap(true);
		panel_info.setWrapStyleWord(true); 
		
		int i=0;
		for(i=0;i<20;i++)
		{ 
			if(info_principal[i][0]!=null)
			{
				panel_info.setText(panel_info.getText()+"\n"+info_principal[i][0]+" : "+info_principal[i][1]);
			}
		}
		
		java.net.URL where2;
		try 
		{
			where2 = new URL(lien_logo);
			ImageIcon logo_compo = new ImageIcon(where2);
			ImagePanel panel_logo_compo = new ImagePanel(logo_compo.getImage());
			panel_info_principal.add(panel_logo_compo,BorderLayout.EAST);
			panel_info_principal.add(panel_info,BorderLayout.WEST);
			
		} catch (MalformedURLException e) 
		{
			e.printStackTrace();
		}
		
		java.net.URL where3;
		try 
		{
			where3 = new URL(lien_image);
			ImageIcon logo_compo = new ImageIcon(where3);
			ImagePanel panel_image_compo = new ImagePanel(logo_compo.getImage());
			panel_info_principal.add(panel_image_compo,BorderLayout.NORTH);
			
		} catch (MalformedURLException e) 
		{
			e.printStackTrace();
		}
		
		JLabel titre_description = new JLabel();
		titre_description.setFont(police_titre);
		titre_description.setPreferredSize(new Dimension(500, 30));
		titre_description.setForeground(Color.black); 
		titre_description.setText("Description");
		
		JTextArea panel_description = new JTextArea();
		panel_description.setFont(police_base);
		panel_description.setPreferredSize(new Dimension(500, 80));
		panel_description.setForeground(Color.black); 
		panel_description.setLineWrap(true);
		panel_description.setWrapStyleWord(true);
		
		i=0;
		for(i=0;i<20;i++)
		{ 
			if(description[i][0]!= null && description[i][0].length()>=6)
			{
				panel_description.setText(panel_description.getText()+"\n"+description[i][0]+" "+description[i][1]);
			}
		}
		
		//panel_description.setText(panel_description.getText()+"\n"+description[0][0]+" "+description[0][1]);
		
		JLabel titre_coordonnees = new JLabel();
		titre_coordonnees.setFont(police_titre);
		titre_coordonnees.setPreferredSize(new Dimension(500, 30));
		titre_coordonnees.setForeground(Color.black); 
		titre_coordonnees.setText("Coordonnees");
		
		JTextArea panel_coordonnees = new JTextArea();
		panel_coordonnees.setFont(police_base);
		panel_coordonnees.setPreferredSize(new Dimension(500, 200));
		panel_coordonnees.setForeground(Color.black); 
		panel_coordonnees.setLineWrap(true);
		panel_coordonnees.setWrapStyleWord(true); 
		
		i=0;
		for(i=0;i<20;i++)
		{ 
			if(coordonnees[i][0]!=null && coordonnees[i][0].length()>=7)
			{
				panel_coordonnees.setText(panel_coordonnees.getText()+"\n"+coordonnees[i][0]);
			}
			else if(coordonnees[i][1]!=null && coordonnees[i][1].length()>=8)
			{
				panel_coordonnees.setText(panel_coordonnees.getText()+" "+coordonnees[i][1]);
			}
		}
		
		gauche.setBackground(Color.white);
    	gauche.setPreferredSize(new Dimension(500, 630));
    	gauche.add(titre,BorderLayout.NORTH);
    	gauche.add(panel_info_principal,BorderLayout.NORTH);
    	gauche.add(titre_description,BorderLayout.NORTH);
    	gauche.add(panel_description,BorderLayout.NORTH);
    	gauche.add(titre_coordonnees,BorderLayout.NORTH);
    	gauche.add(panel_coordonnees,BorderLayout.NORTH);
		
    	JPanel panel_uvsq = new JPanel();
		panel_uvsq.setBackground(Color.white);
		panel_uvsq.setPreferredSize(new Dimension(530, 280));
		
		java.net.URL where;
		try 
		{
			where = new URL("http://www.uvsq.fr/images/logo.png");
			ImageIcon logo = new ImageIcon(where);
			ImagePanel logo_uvsq = new ImagePanel(logo.getImage());
			panel_uvsq.add(logo_uvsq,BorderLayout.CENTER);
		} catch (MalformedURLException e) 
		{
			e.printStackTrace();
		}
		
		ParserUVSQ p = new ParserUVSQ();
		JTextArea info_UVSQ = new JTextArea();
		info_UVSQ.setFont(police_base);
		info_UVSQ.setPreferredSize(new Dimension(500, 200));
		info_UVSQ.setForeground(Color.black); 
		info_UVSQ.setLineWrap(true);
		info_UVSQ.setWrapStyleWord(true);
		info_UVSQ.setText(p.getInfos());
		panel_uvsq.add(info_UVSQ,BorderLayout.NORTH);
		
		RechercheRDF temp = new RechercheRDF(composante_rechercher);
		composante1 = temp.getComposante(composante_rechercher).get(0);
		//composante1 = "Facult� de droit et de science politique (DSP)";
		ParserComposante c1 = new ParserComposante(composante1);
		JPanel panel_composante1 = new JPanel();
		panel_composante1.setBackground(Color.white);
		panel_composante1.setPreferredSize(new Dimension(170, 280));
		JTextArea nom_composante1= new JTextArea(c1.getComposante_rechercher_affichage());
		nom_composante1.setPreferredSize(new Dimension(170, 180));
		nom_composante1.setLineWrap(true);
		nom_composante1.setWrapStyleWord(true); 
		JButton button_composante1 = new JButton();
		button_composante1.setPreferredSize(new Dimension(150, 30));
		button_composante1.setText("ACCEDER");
		button_composante1.addActionListener(new composante1Listener());
		panel_composante1.add(nom_composante1);
		panel_composante1.add(button_composante1);
		
		composante2 = temp.getComposante(composante_rechercher).get(1);
		//composante2 = "UFR des sciences de la sant� Simone Veil";
		ParserComposante c2 = new ParserComposante(composante2);
		JPanel panel_composante2 = new JPanel();
		panel_composante2.setBackground(Color.white);
		panel_composante2.setPreferredSize(new Dimension(170, 280));
		JTextArea nom_composante2= new JTextArea(c2.getComposante_rechercher_affichage());
		nom_composante2.setPreferredSize(new Dimension(170, 180));
		nom_composante2.setLineWrap(true);
		nom_composante2.setWrapStyleWord(true);
		JButton button_composante2 = new JButton();
		button_composante2.setPreferredSize(new Dimension(150, 30));
		button_composante2.setText("ACCEDER");
		button_composante2.addActionListener(new composante2Listener());
		panel_composante2.add(nom_composante2);
		panel_composante2.add(button_composante2);
		
		composante3 = temp.getComposante(composante_rechercher).get(2);
		//composante3 ="IUT de Mantes en Yvelines";
		ParserComposante c3 = new ParserComposante(composante3);
		JPanel panel_composante3 = new JPanel();
		panel_composante3.setBackground(Color.white);
		panel_composante3.setPreferredSize(new Dimension(170, 280));
		JTextArea nom_composante3= new JTextArea(c3.getComposante_rechercher_affichage());
		nom_composante3.setPreferredSize(new Dimension(170, 180));
		nom_composante3.setLineWrap(true);
		nom_composante3.setWrapStyleWord(true);
		JButton button_composante3 = new JButton();
		button_composante3.setPreferredSize(new Dimension(150, 30));
		button_composante3.setText("ACCEDER");
		button_composante3.addActionListener(new composante3Listener());
		panel_composante3.add(nom_composante3);
		panel_composante3.add(button_composante3);
		
		JPanel panel_prof = new JPanel();
		//panel_prof.setBackground(Color.white);
		panel_prof.setPreferredSize(new Dimension(530, 280));
		panel_prof.add(panel_composante1,BorderLayout.EAST);
		panel_prof.add(panel_composante2,BorderLayout.EAST);
		panel_prof.add(panel_composante3,BorderLayout.EAST);
    	
    	//droite.setBackground(Color.white);
    	droite.setPreferredSize(new Dimension(530, 640));
    	droite.add(panel_uvsq,BorderLayout.NORTH);
    	droite.add(panel_prof,BorderLayout.NORTH);
    	
		//resultat.setBackground(Color.white);
    	resultat.setPreferredSize(new Dimension(1050, 650));
    	resultat.add(gauche,BorderLayout.EAST);
    	resultat.add(droite,BorderLayout.WEST);
		return resultat;
    }
	
	class composante1Listener implements ActionListener
    {
        public void actionPerformed(ActionEvent e) 
        {
			try {
				resultat.removeAll();
				ParserComposante c1;
				System.out.println("NOUVELLE PAGE "+composante1);
				c1 = new ParserComposante(composante1);
				c1.etude();
				JPanel panelNewComposante = new JPanel();
				panelNewComposante = c1.resultat();
				BorderLayout test = new BorderLayout();
				resultat.add(panelNewComposante,test);
				resultat.validate();
				resultat.repaint();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (XPatherException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
        }
    }
	class composante2Listener implements ActionListener
    {
        public void actionPerformed(ActionEvent e) 
        {
			try {
				resultat.removeAll();
				ParserComposante c2;
				System.out.println("NOUVELLE PAGE "+composante2);
				c2 = new ParserComposante(composante2);
				c2.etude();
				JPanel panelNewComposante = new JPanel();
				panelNewComposante = c2.resultat();
				BorderLayout test = new BorderLayout();
				resultat.add(panelNewComposante,test);
				resultat.validate();
				resultat.repaint();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (XPatherException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
        }
    }
	class composante3Listener implements ActionListener
    {
         public void actionPerformed(ActionEvent e) 
         {
			try {
				resultat.removeAll();
				ParserComposante c3;
				System.out.println("NOUVELLE PAGE "+composante3);
				c3 = new ParserComposante(composante3);
				c3.etude();
				JPanel panelNewComposante = new JPanel();
				panelNewComposante = c3.resultat();
				BorderLayout test = new BorderLayout();
				resultat.add(panelNewComposante,test);
				resultat.validate();
				resultat.repaint();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (XPatherException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
         }
    }
	
	public static String[][] getInfo_principal() {
		return info_principal;
	}

	public static void setInfo_principal(String[][] info_principal) {
		ParserComposante.info_principal = info_principal;
	}

	public static String[][] getDescription() {
		return description;
	}

	public static void setDescription(String[][] description) {
		ParserComposante.description = description;
	}

	public static String[][] getCoordonnees() {
		return coordonnees;
	}

	public static void setCoordonnees(String[][] coordonnees) {
		ParserComposante.coordonnees = coordonnees;
	}

	public static String getComposante_rechercher() {
		return composante_rechercher;
	}

	public static void setComposante_rechercher(String composante_rechercher) {
		ParserComposante.composante_rechercher = composante_rechercher;
	}

	public static String getLien_final() {
		return lien_final;
	}

	public static void setLien_final(String lien_final) {
		ParserComposante.lien_final = lien_final;
	}

	public static String getLien_site_spe() {
		return lien_site_spe;
	}

	public static void setLien_site_spe(String lien_site_spe) {
		ParserComposante.lien_site_spe = lien_site_spe;
	}

	public static String getLien_logo() {
		return lien_logo;
	}

	public static void setLien_logo(String lien_logo) {
		ParserComposante.lien_logo = lien_logo;
	}

	public static String getLien_image() {
		return lien_image;
	}

	public static void setLien_image(String lien_image) {
		ParserComposante.lien_image = lien_image;
	}

	public static String getLienComposantes() {
		return LienComposantes;
	}

	public static void setLienComposantes(String lienComposantes) {
		LienComposantes = lienComposantes;
	}

	public static String getComposante_rechercher_affichage() {
		return composante_rechercher_affichage;
	}

	public static void setComposante_rechercher_affichage(
			String composante_rechercher_affichage) {
		ParserComposante.composante_rechercher_affichage = composante_rechercher_affichage;
	}
}
